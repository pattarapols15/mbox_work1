package com.example.pattarapol.test5;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class MainActivity extends AppCompatActivity {


    Map<String, ArrayList<Integer>> Key_store = new HashMap<>();

    int width_screen, height_screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FilePickerBuilder.getInstance().setMaxCount(1)
                .addFileSupport("XML", new String[]{"xml", "XML"}, 0)
                .enableDocSupport(false)
                .setActivityTheme(R.style.AppTheme)
                .pickFile(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        width_screen = metrics.widthPixels;
        height_screen = metrics.heightPixels;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {



                    LinearLayout ll2 = (LinearLayout) findViewById(R.id.ll_main);

                    String s = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS).toString();
                    String path = s.substring(1, s.length() - 1);
//                    Log.i("File", path);
                    try {
                        File file = new File(path);
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        String line;
                        int key_count = 1;
                        int pageview = 1;
                        while ((line = br.readLine()) != null) {
                            ScrollView sv = new ScrollView(this);
                            LinearLayout ll = new LinearLayout(this);
                            ll.setOrientation(LinearLayout.VERTICAL);
                            sv.addView(ll);
                            if (line.matches("(.*)<page>(.*)")) {
                                Log.i("pass","pass");
                                while ((line = br.readLine()) != null) {
                                    if (line.matches("(.*)<tv>(.*)")) {

                                        TextView tv = new TextView(this);
                                        while ((line = br.readLine()) != null) {
                                            if (line.matches("(.*)id=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("id=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                tv.setId(key_count);
                                                ArrayList<Integer> values = new ArrayList<Integer>();
                                                values.add(key_count);
                                                values.add(1);
                                                Key_store.put(temp_t[1], values);
                                                key_count++;
                                            } else if (line.matches("(.*)text=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("text=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                tv.setText(temp_t[1]);

                                            } else if (line.matches("(.*)size=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("size=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                tv.setTextSize(Integer.parseInt(temp_t[1]));

                                            } else if (line.matches("(.*)color=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("color=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                tv.setTextColor(Color.parseColor(temp_t[1]));

                                            } else if (line.matches("(.*)</tv>(.*)")) {
                                                break;
                                            }
                                        }
                                        ll.addView(tv);
                                    } else if (line.matches("(.*)<rg>(.*)")) {
                                        RadioGroup rg = new RadioGroup(this);
                                        while ((line = br.readLine()) != null) {
                                            if (line.matches("(.*)id=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("id=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                rg.setId(key_count);
                                                ArrayList<Integer> values = new ArrayList<Integer>();
                                                values.add(key_count);
                                                values.add(2);
                                                Key_store.put(temp_t[1], values);
                                                key_count++;
                                            } else if (line.matches("(.*)item=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("item=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                RadioButton rbn = new RadioButton(this);
                                                rbn.setId(key_count);
                                                rbn.setText(temp_t[1]);
                                                rg.addView(rbn);
                                                key_count++;

                                            } else if (line.matches("(.*)</rg>(.*)")) {
                                                break;
                                            }
                                        }
                                        ll.addView(rg);
                                    } else if (line.matches("(.*)<sp>(.*)")) {
                                        Spinner spinner = new Spinner(this);
                                        ArrayList<String> spinnerArray = new ArrayList<String>();
                                        while ((line = br.readLine()) != null) {
                                            if (line.matches("(.*)id=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("id=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                spinner.setId(key_count);
                                                ArrayList<Integer> values = new ArrayList<Integer>();
                                                values.add(key_count);
                                                values.add(3);
                                                Key_store.put(temp_t[1], values);
                                                key_count++;
                                            } else if (line.matches("(.*)item=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("item=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                spinnerArray.add(temp_t[1]);
                                            } else if (line.matches("(.*)</sp>(.*)")) {
                                                break;
                                            }
                                        }
                                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
                                        spinner.setAdapter(spinnerArrayAdapter);
                                        ll.addView(spinner);

                                    } else if (line.matches("(.*)<ed>(.*)")) {
                                        EditText edttext = new EditText(this);
                                        while ((line = br.readLine()) != null) {
                                            if (line.matches("(.*)id=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("id=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                edttext.setId(key_count);
                                                ArrayList<Integer> values = new ArrayList<Integer>();
                                                values.add(key_count);
                                                values.add(4);
                                                Key_store.put(temp_t[1], values);
                                                key_count++;
                                            } else if (line.matches("(.*)text=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("text=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                edttext.setHint(temp_t[1]);

                                            } else if (line.matches("(.*)</ed>(.*)")) {
                                                break;
                                            }
                                        }
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                                android.widget.LinearLayout.LayoutParams.MATCH_PARENT,
                                                WRAP_CONTENT);
                                        ll.addView(edttext, params);

                                    } else if (line.matches("(.*)<im>(.*)")) {
                                        ImageView Image = new ImageView(this);
                                        while ((line = br.readLine()) != null) {
                                            if (line.matches("(.*)id=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("id=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                Image.setId(key_count);
                                                ArrayList<Integer> values = new ArrayList<Integer>();
                                                values.add(key_count);
                                                values.add(5);
                                                Key_store.put(temp_t[1], values);
                                                key_count++;
                                            } else if (line.matches("(.*)url=(.*)")) {

                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("url=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                Picasso.with(this)
                                                        .load(temp_t[1])
                                                        .error(R.drawable.error)
                                                        .resize(width_screen - 20, (height_screen / 2) - 100)
                                                        .centerInside()
                                                        .onlyScaleDown()
                                                        .into(Image);

                                            } else if (line.matches("(.*)</im>(.*)")) {
                                                break;
                                            }
                                        }
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                                LinearLayout.LayoutParams.MATCH_PARENT,
                                                LinearLayout.LayoutParams.WRAP_CONTENT);
//                                Image.setScaleType(FIT_CENTER);
//                                Image.setPadding(10, 5, 10, 5);
                                        ll.addView(Image, params);

                                    } else if (line.matches("(.*)<bt>(.*)")) {
                                        Button bt = new Button(this);
                                        while ((line = br.readLine()) != null) {
                                            if (line.matches("(.*)id=(.*)")) {
                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("id=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                bt.setId(key_count);
                                                ArrayList<Integer> values = new ArrayList<Integer>();
                                                values.add(key_count);
                                                values.add(6);
                                                Key_store.put(temp_t[1], values);
                                                key_count++;
                                            } else if (line.matches("(.*)text=(.*)")) {
                                                String temp_t[] = new String[2];
                                                int count_t = 0;
                                                for (String retval_t : line.split("text=", 2)) {
                                                    temp_t[count_t] = retval_t;
                                                    count_t++;
                                                }
                                                bt.setText(temp_t[1]);
                                            } else if (line.matches("(.*)event=(.*)")) {
                                                final String finalLine = line;
                                                bt.setOnClickListener(new View.OnClickListener() {
                                                    public void onClick(View view) {
                                                        String temp_t[] = new String[2];
                                                        int count_t = 0;
                                                        for (String retval_t : finalLine.split("event=", 2)) {
                                                            temp_t[count_t] = retval_t;
                                                            count_t++;
                                                        }
                                                        String test1 = "";
                                                        for (String retval_t : temp_t[1].split(",", 0)) {
                                                            ArrayList<Integer> x = Key_store.get(retval_t);
                                                            switch (x.get(1)) {
                                                                case 2:

                                                                    RadioGroup rg = (RadioGroup) findViewById(x.get(0));

                                                                    if (rg.getCheckedRadioButtonId() != -1) {
                                                                        int id = rg.getCheckedRadioButtonId();
                                                                        View radioButton = rg.findViewById(id);
                                                                        int radioId = rg.indexOfChild(radioButton);
                                                                        RadioButton btn = (RadioButton) rg.getChildAt(radioId);
                                                                        test1 = test1 + " , " + btn.getText().toString();

                                                                        Log.i("type", retval_t + x.get(0).toString() + x.get(1).toString() + btn.getText().toString());
                                                                    }
                                                                    break;
                                                                case 3:

                                                                    Spinner sp = (Spinner) findViewById(x.get(0));
                                                                    test1 = test1 + " , " + sp.getSelectedItem().toString();
                                                                    Log.i("type", retval_t + x.get(0).toString() + x.get(1).toString() + sp.getSelectedItem().toString());
                                                                    break;
                                                                case 4:

                                                                    EditText ed = (EditText) findViewById(x.get(0));
                                                                    test1 = test1 + " , " + ed.getText().toString();
                                                                    Log.i("type", retval_t + x.get(0).toString() + x.get(1).toString() + ed.getText().toString());
                                                                    break;
                                                            }
                                                        }
                                                        Toast.makeText(view.getContext(),
                                                                "Data = " + test1, Toast.LENGTH_SHORT)
                                                                .show();
                                                    }
                                                });

                                            } else if (line.matches("(.*)</bt>(.*)")) {
                                                break;
                                            }
                                        }
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                                LinearLayout.LayoutParams.MATCH_PARENT,
                                                LinearLayout.LayoutParams.WRAP_CONTENT);
                                        ll.addView(bt, params);
                                    } else if (line.matches("(.*)</page>(.*)")) {
                                        pageview++;
                                        break;
                                    }
                                }
                            }
                            ll2.addView(sv);
                        }

                        br.close();
                        file = null;

                    } catch (Exception e) {

                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "Failed! = " + e.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }
}

